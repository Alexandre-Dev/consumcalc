# ConsumptionCalc

## Overview
ConsumptionCalc is a simple web-based application designed to calculate and track the fuel consumption of a vehicle over time. Users can input the distance traveled and the amount of fuel consumed, and the application will calculate the average fuel consumption per 100 kilometers.

## Features
- **User Input**: Allows users to enter the distance traveled and the fuel consumption.
- **Average Calculation**: Calculates the average fuel consumption based on the data entered.
- **Data Storage**: Saves the input data in the browser's local storage for persistence.
- **Data Display**: Shows a table with the history of entered distances and consumptions.

## Technologies Used
- HTML5
- Tailwind CSS (for styling)
- JavaScript (for functionality)

## How to Use
1. Enter the distance traveled in the 'Distance' input field.
2. Enter the fuel consumed in the 'Consumption' input field.
3. Click the 'Calculate' button to add the data to the table and update the average consumption.
4. The average consumption will be displayed in liters per 100 kilometers (l/100km).

## Local Storage
The application uses the browser's local storage to keep track of the data entered by the user. This means that the data will persist even after the browser is closed, and will be available upon the next visit.

## Running the Application
To run the application, simply open the HTML file in a web browser. No additional setup is required.

## License
This project is open-source and available for anyone to use or modify.
